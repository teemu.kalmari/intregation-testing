// TDD -unit testing 


const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to Hex conversions", () => {
        it("Convers the basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // #ff0000
            expect(redHex).to.equal("#ff0000");

            const blueHex = converter.rgbToHex(0, 0, 255); //#0000ff
            expect(blueHex).to.equal("#0000ff"); // blue hex value

        });
    });
});